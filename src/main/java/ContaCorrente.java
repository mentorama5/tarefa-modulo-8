public class ContaCorrente extends Conta {
    private double chequeEspecial;
    private double taxaTributacao=0.05;

    public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
        super(numero, agencia, banco, saldo);
        this.chequeEspecial = chequeEspecial;
    }

    /*@Override
    public String toString() {
        return "ContaCorrente{" +
                "chequeEspecial=" + chequeEspecial +
                '}';
    }*/
    public double getSaldo(){
        return this.saldo;
    }
    public double getChequeEspecial() {
        return chequeEspecial;
    }

    public double limiteDeSaque() {
        return getSaldo()+getChequeEspecial();
    }

    @Override
    public boolean sacar(double valor) {
        if(limiteDeSaque() >= valor) {
            setSaldo(this.saldo - valor);
            System.out.println("Saque executado com sucesso");
            return true;
        }
        else {
            System.out.println("Valor não disponível para saque.");
        }
        return false;
    }

    @Override
    public void tributar(){
        System.out.println("Conta corrente possui taxa de tributação de 5% a.a");
        System.out.println("Exemplo: saldo de R$"+getSaldo());
        System.out.println("A taxa a ser descontada é de R$"+(getSaldo()*taxaTributacao));
    }

}
